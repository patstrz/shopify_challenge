import requests, json, datetime


def number_of_pages():
    url = 'http://shopicruit.myshopify.com/products.json?page=1'
    resp = requests.get(url)
    data = json.loads(resp.content)
    page = 1

    while data['products']:
        page += 1
        url = 'http://shopicruit.myshopify.com/products.json?page=%s' % page
        resp = requests.get(url)
        data = json.loads(resp.content)

    return page - 1


# takes in number of pages and returns  all product variants of keyboards,
# and computers in their own separate lists
def get_product_lists(pages):

    keyboards = []
    computers = []

    for page in range(1,pages+1):
        url = 'http://shopicruit.myshopify.com/products.json?page=%s' % page
        resp = requests.get(url)
        data = json.loads(resp.content)

        for product in data['products']:
            if product['product_type'] == 'Keyboard':
                for variant in product['variants']:
                    keyboards.append(variant)

            elif product['product_type'] == 'Computer':
                for variant in product['variants']:
                    computers.append(variant)

    return keyboards, computers


pages = number_of_pages()

if pages > 0:
    # creates two new lists of all keyboard variants and computer variants
    keyboard_variants, computer_variants = get_product_lists(pages)
else:
    print "no products available to buy"


def get_weights_list():

    num_keyboards = len(keyboard_variants)
    num_computers = len(computer_variants)
    all_weights = []

    if num_keyboards <= num_computers:
        # sorts computer variants by prices ascending
        computer_variants.sort(key=lambda x: float(x['price']))
        for i in range(0, num_keyboards):

            all_weights.append(computer_variants[i]['grams'])
            all_weights.append(keyboard_variants[i]['grams'])

        return all_weights

    elif num_keyboards > num_computers:

        for i in range(0, num_computers):
            keyboard_variants.sort(key=lambda x: float(x['price']))

            all_weights.append(computer_variants[i]['grams'])
            all_weights.append(keyboard_variants[i]['grams'])

        return all_weights


if pages > 0:

    all_weights = get_weights_list()
    weight_total_computers = sum(all_weights)
    now = datetime.datetime.now()
    print 'total weight:', sum(all_weights),'g','or',sum(all_weights)/1000.0,'kg'
    print 'time:',str(now)

else:

    print "no products in store"





