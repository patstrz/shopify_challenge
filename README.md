Gives you an output of weight with a timestamp for a combination of keyboards and computers from the shopicruit API.


**Clone the repo into a virtual env**
->   $ virtualenv name
->   navigate into virtualenv directory
->   $source bin/activate 
->   navigate into project directory
->   $ pip install -r requirements.txt
**run the program:**
->  python main.py

Yay! Now you can figure out how many volunteers you need to carry the equipment :)